import Vue from 'vue'
import App from './App.vue'
import store from './store'

import YmapPlugin from 'vue-yandex-maps'
// import DatePicker from 'vue2-datepicker'
// import 'vue2-datepicker/index.css'
import 'normalize.css/normalize.css'

const settings = {
  apiKey: '22edf78e-bf17-4087-8832-dc167c61d9dd',
  lang: 'ru_RU',
  coordorder: 'latlong',
  version: '2.1'
}

Vue.use(YmapPlugin, settings)

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
