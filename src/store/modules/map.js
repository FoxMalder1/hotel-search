export default {
  state: {
    markers: []
  },
  mutations: {
    addMarkers (state, payload) {
      state.markers.push(payload)
    },
    clearMarkers (state) {
      state.markers = []
    }
  },
  actions: {
    callApi ({ commit }, url) {
      return fetch(url)
        .then(response => {
          console.log(url)
          console.log(response)
          if (response.status !== 200) {
            console.error('Что-то не так!')
            console.table({
              statusCode: response.status,
              statusText: response.statusText
            })
            return
          }
          response
            .json()
            .then(data => {
              console.log(data)
              let tmpArr = []
              console.log(data)
              for (let feature in data.features) {
                tmpArr.push(data.features[feature].geometry.coordinates)
              }
              console.log(tmpArr)
              commit('clearMarkers')
              commit('addMarkers', data)
            })
        })
        .catch((err) => {
          console.log(err)
        })
    }
  }
}
