export default {
  state: {
    isOpenModal: false
  },
  mutations: {
    showModal (state) {
      state.isOpenModal = !state.isOpenModal
    },
    closeModal (state) {
      state.isOpenModal = !state.isOpenModal
    }
  }
}
