import Vue from 'vue'
import Vuex from 'vuex'
import modal from './modules/modal'
import map from './modules/map'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    modal,
    map
  },
  strict: process.env.NODE_ENV !== 'production'
})
