export default {
  callApi (url, params) {
    return fetch(url)
      .then(response => {
        console.log(url)
        if (response.status !== 200) {
          console.error('Что-то не так!')
          console.table({
            statusCode: response.status,
            statusText: response.statusText
          })
          return
        }
        response
          .json()
          .then(data => {
            let tmpArr = []
            console.log(data)
            for (let feature in data.features) {
              tmpArr.push(data.features[feature].geometry.coordinates)
            }
            console.log(tmpArr)
            return tmpArr
          })
      })
      .catch((err) => {
        console.log(err)
      })
  }
}
